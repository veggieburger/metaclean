# Meta Cleaner

Python project to clean meta data off of pictures inserted in particular folder. 

This script is in the very early stages. 

This script can be run as a systemd service or on its own. 

Run the script by going to its directory and entering python3 metaClean.py <flags> 

The following flags are available to use. 

-c or --configure

- This flag configures the file config.yml setting the watched directory, recursivity, and whether this will be run as a systemd service or not

-s or --start

- This flag starts watching the set directory if the configuration paramater *service* is set to false
- If *service* is set to true, the script starts the systemctl daemon. 

-sh or --show

- This flag shows the current configuration of config.yml

-ser or --service
 
- This flag is used for the systemctl daemon, it bypasses all if other setups and just watches the set directory. This flag is not intended to only be used by the systemd daemon. 

Ex. 
python3 metaClean.py -sh -c -s 

This command will 
1. show your current config.yml
2. Allow you to edit config.yml
3. Start the service. Ether as a deomon or standalone service. All depending what you configure it as. 

# PyPI Install
pip install metaClean

https://pypi.org/project/metaClean/#description

# Future Work
1. Add the ability to monitor multiple folders for multiple files. Ex Photos, PDF's, Word Docs etc. 
2. Integrate mat2's libraries for more extensive and vetted meta cleaning
3. Add in phone functionality. Make the script clean all meta data for every photo taken by a phone. 





